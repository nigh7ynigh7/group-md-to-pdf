# Markdown to PDF

The `bash.sh` script simply groups the code.

# Run it

It runs on both windows, linux, and mac.
To run the script, execute:

    bash bash.sh

Note: On windows, you need special tools like the linux subsystem or git's bash.

# After that...

Feel free to use the code in the folder/directory `converter` on this project's root.