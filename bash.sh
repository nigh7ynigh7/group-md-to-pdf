output_file="$PWD/temp.md"

if [ -f $output_file ]
then
    rm $output_file
fi


find "$PWD/source" -iname '*.md*' -print0 | sort | while IFS= read -r -d '' file; do
    cat "$file" >> $output_file
    echo -e '\n\n' >> $output_file
done