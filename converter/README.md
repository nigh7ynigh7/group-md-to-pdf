# Intro

A simple converter from markdown to pdf.

This little project uses `wkhtmltopdf` to make the documents.

## Downloading wkhtmltopdf

Note: This is only configured to run with windows.

### Windows

1. Go to their website.
1. Download their zipped package
1. Unzip both `bin` and `include` directories to `CONVERTER_ROOT/wkhtmltopdf/win/`

## Dependencies

Execute:

`npm install`

## Make sure...

That right above this directory, a file called `temp.md` exists.
If it doesn't the code will just ignore you.

# Run

Execute

    node ./index.js

or

    node .

