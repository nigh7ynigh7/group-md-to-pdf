const fs = require('fs');
const path = require('path');

var filename = path.join(__dirname, '..', 'temp.md')

if(!fs.existsSync(filename)){
    console.log('temp.md does not exist');
    return process.exit(1);
}

// load html
var model = fs.readFileSync(path.join(__dirname, 'model.html'), {encoding : 'utf-8'});

// html to pdf
const showdown = require('showdown');
var converter = new showdown.Converter();
var htmlTemp = path.join(__dirname, 'temp.html');
model = model.replace('{{BODY_PLACEHOLDER}}', converter.makeHtml(fs.readFileSync(filename, {encoding : 'utf-8'})))
             .replace('{{STYLE}}', fs.readFileSync(path.join(__dirname, 'style.css')))
fs.writeFileSync(htmlTemp, model);


const os = require('os')
const cp = require('child_process')

var program = __dirname

if(os.type() == 'Windows_NT'){
    program = path.join(program, 'wkhtmltopdf/win/bin/wkhtmltopdf.exe')
}
else{
    console.log('OS not supported');
    return process.exit(1)
}

var final = path.join(__dirname, 'final.pdf')

cp.spawnSync(program, [ '-s', 'A4', htmlTemp, final ]);

fs.unlinkSync(htmlTemp);